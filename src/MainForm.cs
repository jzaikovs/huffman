﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;

namespace Huffman
{
    public sealed partial class MainForm : Form
    {
        private int _nodeWidth = 40;
        private int _nodeHeight = 40;
        private float _linesW = 1.0f;
        private const int padding = 16;
        private const int levelPadding = 16;

        private Color _liefColor = Color.SteelBlue;
        private Color _nodeColor = Color.Tan;

        private readonly Font _font;

        private HuffmanTree Tree { get; set; }

        private readonly string _title = string.Empty;

        public MainForm()
        {
            InitializeComponent();
            _title = Text;
            _font = new Font("Tahoma", 10.0f);
            Tree = new HuffmanTree();
            textToCode.Text = @"Tik, tak, tik, tak, tāda laika taktika!";
        }

        private void textChanged(object sender, EventArgs e)
        {
            var stopW = new Stopwatch();
            stopW.Start();
            UpdateTable();
            stopW.Stop();
            Text = string.Format("{0} [{1}ms]", _title, stopW.ElapsedMilliseconds);
        }

        private void UpdateTable()
        {
            encodedBinary.Text = string.Empty;

            //sakam parbaudiit koku
            if (textToCode.Text.Length > 0)
            {
                simboluTabula.Clear();

                Tree.Encode(textToCode.Text);

                Tree.SortCodes();

                foreach (RedundancRecord c in Tree.Codes)
                    simboluTabula.Text += string.Format("'{0}'\t{1}\t{2}{3}", c.A, c.W, c.C, Environment.NewLine);

                string buffer = string.Empty;

                for (int i = 0; i < textToCode.Text.Length; i++)
                {
                    char a = textToCode.Text[i];

                    foreach (var c in Tree.Codes)
                    {
                        if (c.A != a) continue;

                        buffer += c.C;
                        break;
                    }
                }

                encodedBinary.Text = buffer;

                int l = buffer.Length;

                if (l%8 > 0)
                    l = (l/8) + 1;
                else
                    l = l/8;

                simboluTabula.Text += string.Format("nekompresēts: {0} baiti\r\nkompresēts: ~{1} baiti",
                                                    textToCode.Text.Length, l);
            }

            StartDrawingTree();
        }

        private void StartDrawingTree()
        {
            UpdateSettings();

            int liefCount = Tree.CountLiefsInBranch(Tree.RootNode);

            int longestPath = 0; //atradoam garāko ceļu
            foreach (var cod in Tree.Codes)
                if (cod.C.Length > longestPath)
                    longestPath = cod.C.Length;

            TreePicture.Size = new Size(Math.Max(liefCount*(_nodeWidth + padding), 640),
                                        Math.Max((longestPath + 1)*(_nodeHeight + levelPadding) + 32, 480));
            TreePicture.Image = new Bitmap(TreePicture.Width, TreePicture.Height);

            var g = Graphics.FromImage(TreePicture.Image);
            g.Clear(Color.White);
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            if (liefCount > 0 && textToCode.Text.Length > 0)
            {
                Draw(g, Tree.RootNode, TreePicture.Image.Width/2, 0, longestPath, liefCount);
            }

            TreePicture.Refresh();
        }

        private void Draw(Graphics g, Node node, int parentX, int level, int deep, int parentL)
        {
            if (node == null) return;

            string text = node is LiefNode ? string.Format("'{0}'\n", (node as LiefNode).A) : node.W.ToString();

            int textW = (int) g.MeasureString(text, _font).Width;
            int textH = (int) g.MeasureString(text, _font).Height;

            if (node is LiefNode)
            {
                int y = deep*(_nodeHeight + levelPadding) + 10;
                g.DrawEllipse(new Pen(_liefColor, _linesW), parentX - _nodeWidth/2, y, _nodeWidth, _nodeHeight);
                g.DrawString(text, _font, Brushes.Black, parentX - textW/2, y + (_nodeHeight - textH)/2);

                text = node.W.ToString();
                textW = (int) g.MeasureString(text, _font).Width;
                g.DrawString(text, _font, Brushes.Black, parentX - textW/2, y + _nodeHeight);
            }

            if (node is InternalNode)
            {
                int y = level*(_nodeHeight + levelPadding) + 10;
                g.DrawEllipse(new Pen(_nodeColor, _linesW), parentX - _nodeWidth/2, y, _nodeWidth, _nodeHeight);
                g.DrawString(text, _font, Brushes.Black, parentX - textW/2, y + (_nodeHeight - textH)/2);

                int lieftL = Tree.CountLiefsInBranch((node as InternalNode).NodeLeft);
                int lieftR = parentL - lieftL;

                int liefts = lieftL + lieftR;

                int width = liefts*(_nodeWidth + padding); //bildes platums;

                int start = parentX - width/2;
                int end = parentX + width/2;

                int line = start + (lieftL*(_nodeWidth + padding));

                int nextX1 = start + (line - start)/2;

                int nextX2 = end - (end - line)/2;


                y += (_nodeHeight/2);

                int tempLevel = (node as InternalNode).NodeLeft is LiefNode ? deep : level + 1;
                g.DrawLine(new Pen(Brushes.Gray, 1f), parentX - _nodeWidth/2, y, nextX1, y);
                g.DrawLine(new Pen(Brushes.Gray, 1f), nextX1, y, nextX1, tempLevel*(_nodeHeight + levelPadding) + 10);

                tempLevel = (node as InternalNode).NodeRight is LiefNode ? deep : level + 1;
                g.DrawLine(new Pen(Brushes.Gray, 1f), parentX + _nodeWidth/2, y, nextX2, y);
                g.DrawLine(new Pen(Brushes.Gray, 1f), nextX2, y, nextX2, tempLevel*(_nodeHeight + levelPadding) + 10);

                Draw(g, (node as InternalNode).NodeLeft, nextX1, level + 1, deep, lieftL);
                Draw(g, (node as InternalNode).NodeRight, nextX2, level + 1, deep, lieftR);
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                TreePicture.Image.Save(saveFileDialog1.FileName, System.Drawing.Imaging.ImageFormat.Png);
            }
        }

        private void SelectColors(object sender, EventArgs e)
        {
            krasas.Color = ((Button) sender).BackColor;

            if (krasas.ShowDialog() == DialogResult.OK)
            {
                ((Button) sender).BackColor = krasas.Color;

                StartDrawingTree();
            }

            textToCode.Select();
        }

        private void NumericChanged(object sender, EventArgs e)
        {
            StartDrawingTree();
        }

        private void UpdateSettings()
        {
            _nodeWidth = (int) numericUpDown2.Value;

            _nodeHeight = (int) numericUpDown3.Value;

            _linesW = (float) numericUpDown1.Value;

            _liefColor = buttonLiefColor.BackColor;
            _nodeColor = buttonNodeColor.BackColor;
        }
    }
}
