﻿namespace Huffman
{
    public class Node
    {
        public int W { get; set; }
    }

    public class LiefNode : Node
    {
        public char A { get; set; }
    }

    public class InternalNode : Node
    {
        public Node NodeRight { get; set; }
        public Node NodeLeft { get; set; }
    }
}
