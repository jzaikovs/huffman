﻿using System.Collections.Generic;
using System.Diagnostics;

namespace Huffman
{
    public class HuffmanTree
    {
        public Node RootNode { get; set; }

        public List<RedundancRecord> Codes { get; set; }

        public HuffmanTree()
        {
            RootNode = null;
        }

        private List<Node> CreateLiefs(string text)
        {
            var nodes = new List<Node>();

            if (text.Length == 0) return nodes;

            for (int i = 0; i < text.Length; i++)
            {
                bool contain = false;

                foreach (LiefNode lNode in nodes)
                {
                    if (lNode.A != text[i]) continue;

                    lNode.W++;
                    contain = true;
                    break;
                }

                if (contain) continue;

                var temp = new LiefNode {A = text[i], W = 1};
                nodes.Add(temp);
            }

            return nodes;
        }

        public void Encode(string text)
        {
            if (string.IsNullOrEmpty(text)) return;

            var stopW = new Stopwatch();
            stopW.Start();

            var m = CreateLiefs(text);

            while (m.Count > 1)
            {
                for (int i = 1; i < m.Count; i++)
                {
                    if (m[i].W < m[0].W)
                    {
                        var temp = m[0];
                        m[0] = m[i];
                        m[i] = temp;
                        continue;
                    }

                    if (m[i].W < m[1].W && m[i].W >= m[0].W)
                    {
                        var temp = m[1];
                        m[1] = m[i];
                        m[i] = temp;
                    }
                }

                var inode = new InternalNode {NodeLeft = m[0]};

                m.Remove(m[0]);

                inode.NodeRight = m[0];
                m.Remove(m[0]);

                inode.W = inode.NodeLeft.W + inode.NodeRight.W;
                m.Add(inode);
            }

            Codes = new List<RedundancRecord>();

            if (m.Count > 0)
            {
                DrawTree(m[0], "");

                RootNode = m[0];
            }

            stopW.Stop();
            Debug.WriteLine(string.Format("encode time:{0}", stopW.ElapsedTicks));
        }

        public void SortCodes()
        {
            if (Codes == null) return;

            for (int i = 0; i < Codes.Count - 1; i++)
            {
                for (int j = i + 1; j < Codes.Count; j++)
                {
                    if (Codes[i].W < Codes[j].W)
                    {
                        var temp = Codes[i];
                        Codes[i] = Codes[j];
                        Codes[j] = temp;
                    }
                }
            }
        }

        private void DrawTree(Node node, string s)
        {
            if (node is LiefNode)
            {
                var c = new RedundancRecord {A = (node as LiefNode).A, C = s, W = (node as LiefNode).W};
                Codes.Add(c);
            }

            if (!(node is InternalNode)) return;

            DrawTree((node as InternalNode).NodeLeft, s + "0");
            DrawTree((node as InternalNode).NodeRight, s + "1");
        }

        public int CountLiefsInBranch(Node node)
        {
            if (node is InternalNode)
            {
                return CountLiefsInBranch((node as InternalNode).NodeRight) +
                       CountLiefsInBranch((node as InternalNode).NodeLeft);
            }
            if (node is LiefNode)
            {
                return 1;
            }
            return 0;
        }
    }
}
