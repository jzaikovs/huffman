﻿namespace Huffman
{
    public struct RedundancRecord
    {
        public char A;
        public int W;
        public string C;
    }
}